import psycopg2

def num_of_matches_per_year_by_sql():
    num_of_matches_dict={}
    con = psycopg2.connect(database="postgres", user="postgres",password="123", host="127.0.0.1", port="5432")
    cur = con.cursor()
    cur.execute("SELECT season, COUNT(season) FROM matches GROUP BY season ORDER BY season")
    result=cur.fetchall()
    con.commit()
    con.close()
    for item in result:
        num_of_matches_dict[item[0]]=item[1]
    # print(num_of_matches_dict)
    return num_of_matches_dict

def matches_won_vs_year_by_sql():
    con = psycopg2.connect(database="postgres", user="postgres",password="123", host="127.0.0.1", port="5432")
    cur = con.cursor()
    cur.execute("SELECT season,winner,COUNT(winner)FROM matches WHERE winner IS NOT NULL GROUP BY season,winner ORDER BY season,winner")
    result=cur.fetchall()
    # print(result)
    cur1 = con.cursor()
    cur1.execute('''SELECT DISTINCT winner FROM matches WHERE winner IS NOT NULL''')
    team_list_result=cur1.fetchall()
    con.commit()
    con.close()
    team_list=[]
    for i in team_list_result:
        team_list.append(i[0])
    plot_lst,temp_lst=[],[0]*len(team_list)
    year=result[0][0]
    for item in result:
        yr=item[0]
        if(year==yr):
            temp_lst[team_list.index(item[1])]=item[2]
        else:
            plot_lst.append(temp_lst)
            year=item[0]
            temp_lst=[0]*len(team_list)
            temp_lst[team_list.index(item[1])]=item[2]
    plot_lst.append(temp_lst)
    # print(plot_lst)
    return plot_lst,team_list



def top_bowlers_by_sql():
    top_bowler_dict={}
    con = psycopg2.connect(database="postgres", user="postgres",password="123", host="127.0.0.1", port="5432")
    cur = con.cursor()
    cur.execute("SELECT bowler, (sum(total_runs)/(count(bowler)/6.0)) as rate FROM deliveries INNER JOIN matches ON match_id = id AND matches.season = '2015' GROUP BY deliveries.bowler ORDER BY rate limit 10 ")
    result=cur.fetchall()
    con.commit()
    con.close()
    for item in result:
        top_bowler_dict[item[0]]=item[1]
    # print(top_bowler_dict)
    return top_bowler_dict


def extra_runs_per_team_by_sql():
    extra_runs_dict={}
    con = psycopg2.connect(database="postgres", user="postgres",password="123", host="127.0.0.1", port="5432")
    cur = con.cursor()
    cur.execute("SELECT bowling_team, SUM(extra_runs) FROM deliveries INNER JOIN matches ON match_id=id and matches.season='2016' GROUP BY bowling_team")
    result=cur.fetchall()
    con.commit()
    con.close()
    for item in result:
        extra_runs_dict[item[0]]=item[1]
    # print(extra_runs_dict)
    return extra_runs_dict

def team_performance_by_sql():
    performance_dict={}
    team_name='Royal Challengers Bangalore'
    con = psycopg2.connect(database="postgres", user="postgres",password="123", host="127.0.0.1", port="5432")
    cur = con.cursor()
    cur.execute("SELECT season,COUNT(winner) FROM matches WHERE winner='Royal Challengers Bangalore' GROUP BY season ORDER BY season")
    result=cur.fetchall()
    con.commit()
    con.close()
    for item in result:
        performance_dict[item[0]]=item[1]
    # print(performance_dict)
    return performance_dict

# if __name__=="__main__":
    # num_of_matches_dict=num_of_matches_per_year_by_sql()
    # extra_runs_dict=extra_runs_per_team_by_sql()
    # top_bowler_dict=top_bowlers_by_sql()
    # plot_lst,team_lst=matches_won_vs_year_by_sql()
    # performance_dict=team_performance_by_sql()
