import psycopg2
def create_table_matches():
    con = psycopg2.connect(database="postgres", user="postgres",password="123", host="127.0.0.1", port="5432")
    cur = con.cursor()
    cur.execute('''CREATE TABLE matches(id integer NOT NULL,season varchar,city varchar,date date,team1 varchar,team2 varchar,toss_winner varchar,toss_decision varchar,result varchar,dl_applied integer,winner varchar,win_by_runs integer,win_by_wickets integer,player_of_match varchar,venue varchar,umpire1 varchar,umpire2 varchar,umpire3 varchar);''')
    # print("Table created successfully")
    cur.execute("COPY matches FROM '/home/fasna/Desktop/data_assignment/ipl/matches.csv' DELIMITER ',' CSV HEADER;")
    # print("Table copied successfully")
    con.commit()
    con.close()


def create_table_deliveries():
    con = psycopg2.connect(database="postgres", user="postgres",password="123", host="127.0.0.1", port="5432")
    # print("Database opened successfully")
    cur = con.cursor()
    cur.execute('''CREATE TABLE deliveries
    (match_id SMALLINT,
    inning SMALLINT,
    batting_team TEXT,
    bowling_team TEXT,
    over SMALLINT,
    ball SMALLINT,
    batsman TEXT,
    non_striker TEXT,
    bowler TEXT,
    is_super_over SMALLINT,
    wide_runs SMALLINT,
    bye_runs SMALLINT,
    legbye_runs SMALLINT,
    noball_runs SMALLINT,
    penalty_runs SMALLINT,
    batsman_runs SMALLINT,
    extra_runs SMALLINT,
    total_runs SMALLINT,
    player_dismissed TEXT,
    dismissal_kind TEXT,
    fielder TEXT);''')
    # print("Table created successfully")
    cur.execute("COPY deliveries FROM '/home/fasna/Desktop/data_assignment/ipl/deliveries.csv' DELIMITER ',' CSV HEADER;")
    # print("Table copied successfully")
    con.commit()
    con.close()


if __name__=='__main__':
    # create_table_matches()
    create_table_deliveries()
