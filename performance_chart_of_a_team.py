import csv
from matplotlib import pyplot as plt
import numpy as np
from sql_queries import team_performance_by_sql

def calculate_win_nums(file_path,team_name):
    win_nums_dict={}
    win_nums_dict_sorted={}
    with open(file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        for row in csv_reader:
            if(row['winner']==team_name and row['season'] not in win_nums_dict):
                win_nums_dict[row['season']]=1
            elif(row['winner']==team_name and row['season'] in win_nums_dict):
                win_nums_dict[row['season']]+=1
    year_list=sorted(win_nums_dict)
    for year in year_list:
        win_nums_dict_sorted[year]=win_nums_dict[year]
    print(win_nums_dict_sorted)
    return win_nums_dict_sorted


def plot_win_nums(performance_dict):
    year=[]
    win_nums=[]
    for item in performance_dict:
        year.append(item)
        win_nums.append(performance_dict[item])
    ind = np.arange(len(year))
    plt.plot(year,win_nums)
    plt.ylim([0,max(win_nums)+5])
    plt.yticks(fontsize=12)
    plt.ylabel('performance', fontsize=12)
    plt.xticks(ind, year, fontsize=5, rotation=60)
    plt.xlabel('year', fontsize=12)
    plt.show()



def performance_chart_of_team_per_year(file_path,team_name):
    performance_dict=calculate_win_nums(file_path,team_name)
    performance_dict=team_performance_by_sql()
    plot_win_nums(performance_dict)

if __name__ == '__main__':
    performance_chart_of_team_per_year('ipl/matches.csv','Royal Challengers Bangalore')
