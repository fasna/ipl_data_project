from num_of_mathches_per_year import matches_per_year_dict
import unittest

class matperyear(unittest.TestCase):
   def num_of_match_per_year(self):
       matches_file = "ipl/mock_matches.csv"
       expected_matches_per_year = {'2016': 5, '2015': 5}
       num_of_matches_per_year = matches_per_year_dict(matches_file)
       self.assertEqual(num_of_matches_per_year, expected_matches_per_year)
if __name__ == '__main__':
   unittest.main();
