import csv
from matplotlib import pyplot as plt
import numpy as np
from sql_queries import num_of_matches_per_year_by_sql


def matches_per_year_dict(file_path):
    matches_per_year={}
    with open(file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file,delimiter=',')
        for row in csv_reader:
            if (row['season'].isdigit()):
                if (row['season'] in matches_per_year):
                    matches_per_year[row['season']]+=1
                else:
                    matches_per_year[row['season']]=1
    return matches_per_year

def plot_matches_per_year(result):
    year=[]
    num_matches=[]
    for item in result:
        year.append(item)
        num_matches.append(result[item])
    ind = np.arange(len(year))
    plt.bar(year,num_matches)
    plt.ylim([0,100])
    plt.yticks(fontsize=12)
    plt.ylabel('num of matches', fontsize=12)
    plt.xticks(ind, year, fontsize=5, rotation=60)
    plt.xlabel('year', fontsize=12)
    plt.show()

def calculate_num_of_matches_per_year(file_path):
    matches_per_year=matches_per_year_dict(file_path)
    matches_per_year=num_of_matches_per_year_by_sql()
    plot_matches_per_year(matches_per_year)

if __name__ == '__main__':
    calculate_num_of_matches_per_year('ipl/matches.csv')
