import csv
from matplotlib import pyplot as plt
import numpy as np
from sql_queries import matches_won_vs_year_by_sql

def setup_operation(file_path):
    year_list=[]
    team_list=[]
    teams=[]
    def append_operation(item,lst):
        if item not in lst:
            lst.append(item)
    with open(file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        next(csv_reader)
        for row in csv_reader:
            append_operation(row['team1'][:4].lower(),teams)
            append_operation(row['team2'][:4].lower(),teams)
            append_operation(row['team1'],team_list)
            append_operation(row['team2'],team_list)
            append_operation(row['season'],year_list)
    year_list.sort()
    # print(year_list)
    return team_list,teams,year_list



def matches_won_per_team_on_each_year(file_path,year,matches_won,btm,teams):
    lst=[]
    with open(file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        for row in csv_reader:
            if(row['winner']!=""):
                if(row['season']==year):
                    matches_won[row['winner'][:4].lower()]+=1
        for team in matches_won:
            lst.append(matches_won[team])
        # print(lst)

    return lst

def matches_won_per_team_dict(file_path):
    matches_won={}
    btm=[]
    plot_list=[]
    btm_lst=[]
    team_list,teams,year_list=setup_operation(file_path)
    for year in year_list:
        for team in teams:
            matches_won[team]=0

        matches_won_per_team_on_each_year_list=matches_won_per_team_on_each_year(file_path,year,matches_won,btm,teams)
        plot_list.append(matches_won_per_team_on_each_year_list)
    # print(plot_list)
    return plot_list,team_list


def plot_matches_won_per_team_on_each_year(plot_list,team_list):
    btm=[]
    ind = np.arange(13)
    for item in plot_list:
        if(plot_list.index(item)==0):
            # for i in range(0,len(item)):
            btm=item
            # print(btm)
            plt.bar(ind,item,width=.5)
        else:
            plt.bar(ind,item,bottom=btm,width=.5)
            for i in range(0,len(item)):
                btm[i]+=item[i]
    plt.ylim([0,max(btm)])
    plt.yticks(fontsize=12)
    plt.ylabel('matches won', fontsize=12)
    plt.xticks(ind, team_list, fontsize=5, rotation=60)
    plt.xlabel('team', fontsize=12)
    plt.show()



def calculate_matches_won_by_each_team(file_path):
    plot_list,team_list=matches_won_per_team_dict(file_path)
    print(plot_list,team_list)
    plot_list,team_list=matches_won_vs_year_by_sql()
    plot_matches_won_per_team_on_each_year(plot_list,team_list)

if __name__ == '__main__':
    calculate_matches_won_by_each_team('ipl/matches.csv')
