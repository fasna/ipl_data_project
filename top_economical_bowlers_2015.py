import csv
from matplotlib import pyplot as plt
import numpy as np
from sql_queries import top_bowlers_by_sql

def get_match_id(matches_file_path):
    match_id=[]
    with open(matches_file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file,delimiter=',')
        for row in csv_reader:
            if(row['season']=="2015"):
                match_id.append(row['id'])
    return match_id

def top_economical_bowler(matches_file_path,deliveries_file_path):
    match_id=get_match_id(matches_file_path)
    bowler_totalruns={}
    bowler_totalballs={}
    bowler_rate={}

    with open(deliveries_file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        for row in csv_reader:
            if(row['match_id']in match_id):
                if (row['bowler']in bowler_totalruns):
                    bowler_totalruns[row['bowler']]+=int(row['total_runs'])
                else:
                    bowler_totalruns[row['bowler']]=int(row['total_runs'])
                if (row['bowler']in bowler_totalballs):
                    bowler_totalballs[row['bowler']]+=1
                else:
                    bowler_totalballs[row['bowler']]=1
    for item in bowler_totalruns:
        total_over=bowler_totalballs[item]/6
        economy_rate=bowler_totalruns[item]/total_over
        bowler_rate[item]=economy_rate
    top_bowlers_dict={}
    top_rate_list=[]
    for item in bowler_rate:
        top_rate_list.append(bowler_rate[item])
    top_rate_list.sort()
    for item in bowler_rate:
        if(bowler_rate[item] in top_rate_list[:10]):
            top_bowlers_dict[item]=bowler_rate[item]
    return top_bowlers_dict

def plot_top_economical_bowler(bowler_rate,top_rate_list):
    bowler=[]
    rate=[]
    for item in bowler_rate:
        bowler.append(item)
        rate.append(bowler_rate[item])
    plt.bar(bowler,rate)
    ind = np.arange(len(bowler))
    plt.ylim([0,max(rate)+1])
    plt.yticks(fontsize=12)
    plt.ylabel('rate', fontsize=12)
    plt.xticks(ind, bowler, fontsize=5, rotation=60)
    plt.xlabel('top 10 bowlers', fontsize=12)
    plt.show()

def calculate_top_economical_bowler(matches_file_path,deliveries_file_path):
    bowler_rate=top_economical_bowler(matches_file_path,deliveries_file_path)
    bowler_rate=top_bowlers_by_sql()
    # print(bowler_rate)
    plot_top_economical_bowler(bowler_rate,top_rate_list)



if __name__ == '__main__':
    calculate_top_economical_bowler('ipl/matches.csv','ipl/deliveries.csv')
