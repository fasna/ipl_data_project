import csv
from matplotlib import pyplot as plt
import numpy as np
from sql_queries import extra_runs_per_team_by_sql

def calc_extra_runs(matches_file_path,deliveries_file_path):
    match_id=[]
    extra_runs={}
    with open(matches_file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        for row in csv_reader:
            if(row['season']=="2016"):
                match_id.append(row['id'])

    with open(deliveries_file_path, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file, delimiter=',')
        for row in csv_reader:
            if(row['match_id']in match_id):
                if (row['bowling_team']in extra_runs):
                    extra_runs[row['bowling_team']]+=int(row['extra_runs'])
                else:
                    extra_runs[row['bowling_team']]=int(row['extra_runs'])
    return extra_runs
def plot_extra_runs_per_team(extra_runs):
    team=[]
    runs=[]
    for item in extra_runs:
        team.append(item)
        runs.append(extra_runs[item])
        plt.bar(team,runs)
    ind = np.arange(len(team))
    plt.ylim([0,150])
    plt.yticks(fontsize=12)
    plt.ylabel('extra runs', fontsize=12)
    plt.xticks(ind, team, fontsize=5, rotation=60)
    plt.xlabel('team', fontsize=12)
    plt.show()


def calculate_extraruns_conceded_per_team_2016(matches_file_path,deliveries_file_path):
    extra_runs=calc_extra_runs(matches_file_path,deliveries_file_path)
    extra_runs=extra_runs_per_team_by_sql()
    # print(extra_runs)
    plot_extra_runs_per_team(extra_runs)


if __name__ == '__main__':
    calculate_extraruns_conceded_per_team_2016('ipl/matches.csv','ipl/deliveries.csv')
