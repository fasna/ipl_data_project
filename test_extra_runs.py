from extra_runs_conceded_per_team_2016 import calc_extra_runs
import unittest

class test_extra_runs(unittest.TestCase):
   def test_extra_runs_func(self):
       csv_file1,csv_file2 = "ipl/mock_matches.csv","ipl/mock_deliveries.csv"
       expected_extra_runs = {'mumbai': 3, 'chennai': 2, 'pune': 2, 'kolkata': 4}
       result= calc_extra_runs(csv_file1,csv_file2)
       self.assertEqual(result, expected_extra_runs)
if __name__ == '__main__':
   unittest.main();
