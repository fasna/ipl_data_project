from top_economical_bowlers_2015 import top_economical_bowler
import unittest

class top_eco_bowler(unittest.TestCase):
   def num_of_match_per_year(self):
       matches_file,deliveries_file = "ipl/mock_matches.csv","ipl/mock_deliveries.csv"
       expected_result= {'jadeja': 60.0, 'pandya': 32.0, 'sharma': 45.0, 'zamba': 24.0, 'tahir': 60.0}
       result= top_economical_bowler(matches_file,deliveries_file)
       self.assertEqual(expected_result,result)
if __name__ == '__main__':
   unittest.main();
