from matches_won_vs_team import matches_won_per_team_dict
import unittest

class matches_won(unittest.TestCase):
   def test_matches_won_per_team_dict(self):
       csv_file = "ipl/mock_matches.csv"
       expected_result1,expected_result2= [[1, 2, 1, 1], [2, 0, 0, 3]], ['pune ', 'kolkata', 'chennai', 'mumbai']
       result1,result2 = matches_won_per_team_dict(csv_file)
       self.assertEqual(result1, expected_result1)
       self.assertEqual(result2, expected_result2)
if __name__ == '__main__':
   unittest.main();
